// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";


//TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD4V9rVZX8MGVyumoImhek1zaT2gHR2ZVk",
  authDomain: "mi-primera-app-c069c.firebaseapp.com",
  databaseURL: "https://mi-primera-app-c069c-default-rtdb.firebaseio.com",
  projectId: "mi-primera-app-c069c",
  storageBucket: "mi-primera-app-c069c.appspot.com",
  messagingSenderId: "744115051362",
  appId: "1:744115051362:web:62b84fe01fe41c5ffaaa3b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
 const database = getDatabase(app);
 const auth = getAuth();
 const db = getDatabase();


 // Funcion para Ingresar

var archivo = document.getElementById('archivo');




// Crear usuarios
//     createUserWithEmailAndPassword(auth, email, password)
//   .then((userCredential) => {
//     // Signed in 
//     const user = userCredential.user;
//     set(ref(database, 'users/' + user.uid),{
//         username: username,
//         email: email
        

//     })
//     alert('Usuario creado')
//     // ...
//   })
//   .catch((error) => {
   
//     const errorMessage = error.message;
//     alert(errorMessage);
//     // ..
//   });



//---------------------------------------------------------

// variables o datos
var id="";
var nombre="";
var precio="";
var descripcion="";
var url ="";
var imgNombre="";
var estado="";
var mostrardatos = document.getElementById('mostrarTodo');

// leer valores 

function leerInputs(){
    
        id = document.getElementById("id").value;
    nombre = document.getElementById("nombre").value;
    precio = document.getElementById("precio").value;
    descripcion = document.getElementById('descripcion').value;
    url = document.getElementById('url').value;
    imgNombre = document.getElementById('imgNombre').value
    estado = "habilitado";
  
    
    
   
    }


    function insertDatos(){
        leerInputs();
        if(document.getElementById('id').value == 0 || document.getElementById('nombre').value == 0 || document.getElementById('precio').value == 0 || document.getElementById('descripcion').value == 0 || document.getElementById('url').value ==0 ){
          alert("Te falta seleccionar un campo")
      }else{
         
         set(ref(db,'productos/' + id),{
         nombre: nombre,
         precio:precio,
         descripcion:descripcion,
         url:url,
         imgNombre:imgNombre,
         estado:estado})
         .then((docRef) => {
        
          
          alert("registro exitoso");
          console.log("datos guardados" )
           
          })
          .catch((error) => {
          alert("Error en el registro")
          });
      }
         }



         // Descargar imagen

         //CARGAR IMAGENES
function cargarImagen(){
    // archivo seleccionado
    const file = event.target.files[0];
    const name = event.target.files[0].name;



    const storage = getStorage();

    const storageRef = refS(storage, "imagenes/" + name)

    //file comes from the Blob or File API
    uploadBytes(storageRef, file).then((snapshot)=> {
      document.getElementById("imgNombre").value = name;
      alert("Se cargo el archivo");
    });
  }
  function descargarImagen(){
    archivo = document.getElementById("imgNombre").value;
    // Create a reference to the file we want to download
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/' + archivo);

    // Get the download URL
    getDownloadURL(starsRef)
      .then((url) => {
        document.getElementById("url").value=url;
        document.getElementById("imagen").src=url;
        // Insert url into an <img> tag to "download"
      })
      .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case 'storage/object-not-found':
            console.log("No existe el archivo")
            // File doesn't exist
            break;
          case 'storage/unauthorized':
            console.log("No tiene permisos")
            // User doesn't have permission to access the object
            break;
          case 'storage/canceled':
            console.log("Se cancelo o no tiene internet")
            // User canceled the upload
            break;

          // ...

          case 'storage/unknown':
            console.log("No se que paso :C")
            // Unknown error occurred, inspect the server response
            break;
        }
      });

  }


  // actualizar datos

  function actualizar(){
    if(document.getElementById('id').value == 0 || document.getElementById('nombre').value == 0 || document.getElementById('precio').value == 0 || document.getElementById('descripcion').value == 0 || document.getElementById('url').value ==0 ){
      alert("Te falta seleccionar un campo")
  }else{
   leerInputs();
  update(ref(db,'productos/'+ id),{
         nombre: nombre,
         precio:precio,
         descripcion:descripcion,
         url:url,
         imgNombre:imgNombre,
         estado:estado
  }).then(()=>{
   alert("se realizo actualizacion");
  })
  .catch(()=>{
   alert("causo Erro " + error );
  });
  }
  }
  

  // mostrar los datos 
  function mostrarDatos(){
    if(document.getElementById('id').value == 0){
      alert("El codigo esta vacio")
    }
    else{
   leerInputs();
   console.log("mostrar datos ");
   const dbref = ref(db);
   get(child(dbref,'productos/'+ id)).then((snapshot)=>{
   if(snapshot.exists()){
         nombre = snapshot.val().nombre,
         precio = snapshot.val().precio;
         descripcion = snapshot.val().descripcion;
         url = snapshot.val().url;
         imgNombre = snapshot.val().imgNombre;
         estado = snapshot.val().estado;
   escribirInpust();
  }
   else {
  
   alert("No existe");
   }
  }).catch((error)=>{
   alert("error buscar" + error);
  });
    }
  }


  // mostrar en los inputs
  function escribirInpust(){

    document.getElementById("id").value = id;
    document.getElementById("nombre").value = nombre;
    document.getElementById("precio").value = precio;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('url').value = url;
    document.getElementById('imgNombre').value = imgNombre; 
    document.getElementById("estado").value = estado;
   }

   // deshabilitar producto
   function deshabilitar(){
    if(document.getElementById('id').value == 0){
      alert("El codigo esta vacio")
  }else{
   leerInputs();
   estado = "deshabilitado";
  update(ref(db,'productos/'+ id),{
         estado:estado
  }).then(()=>{
   alert("se realizo actualizacion");
   
  })
  .catch(()=>{
   alert("causo Erro " + error );
  });
  }

   }


   // habilitar producto

   function habilitar(){
    if(document.getElementById('id').value == 0){
      alert("El codigo esta vacio")
  }else{
   leerInputs();
   estado = "habilitado";
  update(ref(db,'productos/'+ id),{
         estado:estado
  }).then(()=>{
   alert("se realizo actualizacion");
   
  })
  .catch(()=>{
   alert("causo Erro " + error );
  });
  }

   }

   // mostrar todos los productos

   function mostrarTodo(){
  
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
     mostrardatos.innerHTML=""
     snapshot.forEach((childSnapshot) => {
     const childKey = childSnapshot.key;
     const childData = childSnapshot.val();
     

    if (childData.estado == "habilitado"){
     mostrardatos.innerHTML = mostrardatos.innerHTML+
     "<div class='product'>"+
     "<h1>" + childKey + "</h1>"+
     "<img class='product-image'"+"src="+childData.url+"' alt= '"+childData.imgNombre+
     "<div class='product-info'>"+
     "<p class='infocenterpro'>"+childData.nombre+"</p>"+
     "<p class='psobremi'>"+childData.descripcion+"</p>"+
     "<span>"+"$"+childData.precio+"</span>"+
     "<br>"+
     "<button class='btn buy'>"+"Buy now"+"</button>"+
     "</div>"+
     "</div>"
    }else{
      mostrardatos.innerHTML = mostrardatos.innerHTML+
     "<div class='product'>"+
     "<h1>" + childKey +"<br>"+"Producto deshabilitado"+ "</h1>"+
     "<img class='product-image'"+"src="+childData.url+"' alt= '"+childData.imgNombre+
     "<div class='product-info'>"+
     "<p class='infocenterpro'>"+childData.nombre+"</p>"+
     "<p class='psobremi'>"+childData.descripcion+"</p>"+
     "<span>"+"$"+childData.precio+"</span>"+
     "<br>"+
     "<button class='btn buy'>"+"Buy now"+"</button>"+
     "</div>"+
     "</div>"


    }
     // ...
     });
    }, {
     onlyOnce: true
    });
    }


//Botonones
var btnInsertar = document.getElementById("ingresar");
var verImagen = document.getElementById('verImagen');
var consultar = document.getElementById('consultar');
var btndeshabilitar = document.getElementById('deshabilitar');
var btnhabilitar = document.getElementById('habilitar');
var btnActualizar = document.getElementById('actualizar');
var btnTodo = document.getElementById('todo')

// List de eventos
btnInsertar.addEventListener('click',insertDatos);
verImagen.addEventListener('click', descargarImagen);
archivo.addEventListener('change' ,cargarImagen);
consultar.addEventListener('click', mostrarDatos);
btndeshabilitar.addEventListener('click', deshabilitar);
btnhabilitar.addEventListener('click', habilitar);
btnActualizar.addEventListener('click', actualizar);
btnTodo.addEventListener('click', mostrarTodo);


