  // Import the functions you need from the SDKs you need
  import { initializeApp } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getAnalytics } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-analytics.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyCllwqsov4sU2_7Tp9VtaDQ4GtOqkIzU7E",
    authDomain: "mi-app-web-9f30a.firebaseapp.com",
    projectId: "mi-app-web-9f30a",
    storageBucket: "mi-app-web-9f30a.appspot.com",
    messagingSenderId: "543707292292",
    appId: "1:543707292292:web:b978de94933f4fbdcd58d1",
    measurementId: "G-SL9MVS7125"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
