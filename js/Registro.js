//Conexión a base de datos
import { initializeApp } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
 import { getFirestore, doc, getDoc, getDocs, collection } from
"https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
 import { getDatabase,onValue,ref,set,get,child,update,remove } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";

import { getStorage, ref as refS,uploadBytes, getDownloadURL } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

import { getAuth, createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";


//TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD4V9rVZX8MGVyumoImhek1zaT2gHR2ZVk",
  authDomain: "mi-primera-app-c069c.firebaseapp.com",
  databaseURL: "https://mi-primera-app-c069c-default-rtdb.firebaseio.com",
  projectId: "mi-primera-app-c069c",
  storageBucket: "mi-primera-app-c069c.appspot.com",
  messagingSenderId: "744115051362",
  appId: "1:744115051362:web:62b84fe01fe41c5ffaaa3b"
};
 // Initialize Firebase
// Initialize Firebase
const app= initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();

signUp.addEventListener("click",(e) => {

    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var username = document.getElementById("username").value;

createUserWithEmailAndPassword(auth, email, password)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;

    set(ref(database, "users/" + user.uid),{
        username: username,
        email: email
    })
    alert("Usuario creado");
    
    // ...
  })
  .catch((error) => {
    
    const errorMessage = error.message;
    
    alert(errorMessage);
    // ..
  });
});