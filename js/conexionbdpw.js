// mostrar todos los productos
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";


//TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD4V9rVZX8MGVyumoImhek1zaT2gHR2ZVk",
  authDomain: "mi-primera-app-c069c.firebaseapp.com",
  databaseURL: "https://mi-primera-app-c069c-default-rtdb.firebaseio.com",
  projectId: "mi-primera-app-c069c",
  storageBucket: "mi-primera-app-c069c.appspot.com",
  messagingSenderId: "744115051362",
  appId: "1:744115051362:web:62b84fe01fe41c5ffaaa3b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
 const database = getDatabase(app);
 const auth = getAuth();
 const db = getDatabase();

var mostrardatos = document.getElementById('mostrarTodo');
function mostrarTodo(){
  
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
     mostrardatos.innerHTML=""
     snapshot.forEach((childSnapshot) => {
     const childKey = childSnapshot.key;
     const childData = childSnapshot.val();
     

    if (childData.estado == "habilitado"){
     mostrardatos.innerHTML = mostrardatos.innerHTML+
     
     "<div class='product'>"+
     "<h1>" + childKey + "</h1>"+
     "<img class='product-image'"+"src="+childData.url+"' alt= '"+childData.imgNombre+
     "<div class='product-info'>"+
     "<p class='infocenterpro'>"+childData.nombre+"</p>"+
     "<p class='psobremi'>"+childData.descripcion+"</p>"+
     "<span>"+"$"+childData.precio+"</span>"+
     "<br>"+
     "<button class='btn buy'>"+"Buy now"+"</button>"+
     "</div>"+
     "</div>"
    }else{
        
    }
     // ...
     });
    }, {
     onlyOnce: true
    });
    }


    var btnTodo = document.getElementById('todo')
    btnTodo.addEventListener('click', mostrarTodo());